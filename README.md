Description
---

This application helps you to execute accuracy estimation experiments for provided time series sequences.
This software measures sensitivity (codenamed "Alpha") and specificity values (codenamed "Beta").

Requirements
---

Must have Java 1.8 JDK installed, other versions may break an app unless run using Intellij IDEA run configurations with Java 1.8+ JDK installed.

Configuring experiment parameters
---

For all related generated sequences configuration refer to source files:
`src/main/kotlin/com/jaguarsoft/ritmintatorius/accurator/Main.kt`
`src/main/kotlin/com/jaguarsoft/ritmintatorius/accurator/measureAlpha.kt`
`src/main/kotlin/com/jaguarsoft/ritmintatorius/accurator/measureBeta.kt`

Sequence files, for which experiments will be executed must be included in `src/main/resources`

In `Main.kt` file we uncomment function depending which kind of experiments we want to carry out.

We execute _Alpha_ experiments when we want to get accuracy estimates regarding how accurately core module recognizes known elements.

We execute _Beta_ experiments when we want to get accuracy estimates regarding how accurately core module identifies false positives by not marking those elements at all.

Paths for where to look for sequence and their index files must be configured.
Don't forget to update index and type strings at the bottom of the class file when switching sequence types (e.g. "white", "a9", "n=100").
These values are used for file path construction.

`measureAlpha.kt` has commented lines for convenient experiment switching, and common scenarios should be configured there.
Those lines describe from where should sequence and index .csv files (containing known inserted element indices) be searched in `src/main/resources` directory.

Default paths are configured to look for files in deeper nested directories, which is more convenient when we have more different data we want to estimate at once.
For simpler path configuration, a path variable can be duplicated with directory/filename parts removed.

Similar configuration is carried out in `measureBeta.kt` file.

Loop parameters should be updated to execute less iterations if complete dataset is not provided.

Running
---

Install dependency locally:

`cd ../ritmintatorius-core/`

`./gradlew publishToMavenLocal`

Then we can run experiments:

`./gradlew run`

After executing this command, experiment results will be displayed in console window.