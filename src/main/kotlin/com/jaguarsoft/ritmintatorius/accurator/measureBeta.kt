package com.jaguarsoft.ritmintatorius.accurator

import com.jaguarsoft.ritmintatorius.accurator.service.AccuracyEstimateService

class BetaCalculations {

    fun measure() {

        val filenames = listOf(
            "white,b=0/$N_COUNT_STRING/white,b=0.csv",
            "white,b=1/$N_COUNT_STRING/white,b=1.csv",
            "white,b=2/$N_COUNT_STRING/white,b=2.csv",
            "white,b=3/$N_COUNT_STRING/white,b=3.csv"
//            "$N_COUNT_STRING/a2/a2,b=0.csv",
//            "$N_COUNT_STRING/a2/a2,b=1.csv",
//            "$N_COUNT_STRING/a2/a2,b=2.csv"
//            "a9/$N_COUNT_STRING/a9,b=0.csv"
        )

        iterateThroughFilenames(filenames)
    }

    private fun iterateThroughFilenames(filenames: List<String>) {
        for (csvFilename in filenames)
            measureAndPrintExtrasystoleAccuracy(csvFilename)
    }

    private fun measureAndPrintExtrasystoleAccuracy(csvFilename: String) {
        println("Extrasystole detection accuracy for $csvFilename data set")
        for (KIterator in listOf(1, 3, 5, 10)) {
            for (epsilonIterator in 9..9) {
                println(" with epsilon=0.$epsilonIterator and K=$KIterator is:")
                println(
                    AccuracyEstimateService(Util().getCalculationVariables(epsilonIterator, KIterator))
                        .measureExtrasystoles(csvFilename, "", ORDINARY_INDICES_FILENAME)
                )
            }
        }
        println("------------------------------------")
    }

    companion object {
        const val N_COUNT_STRING = "without"
        const val ORDINARY_INDICES_FILENAME = "white,b=0/$N_COUNT_STRING/n=100,indices.csv"
//        const val ORDINARY_INDICES_FILENAME = "a9/$N_COUNT_STRING/n=100,indices.csv"
    }

}
