package com.jaguarsoft.ritmintatorius.accurator

import com.jaguarsoft.ritmintatorius.accurator.factory.FileReaderFactory
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser

class CsvToIterableConverter(
    private val csvFormat: CSVFormat = CSVFormat.DEFAULT,
    private val fileReaderFactory: FileReaderFactory = FileReaderFactory()
) {

    fun convert(csvFileName: String): CSVParser = csvFormat.parse(fileReaderFactory.create(csvFileName))

}