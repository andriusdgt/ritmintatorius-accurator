package com.jaguarsoft.ritmintatorius.accurator.service

import com.jaguarsoft.ritmintatorius.accurator.CsvToIterableConverter
import com.jaguarsoft.ritmintatorius.accurator.Util
import com.jaguarsoft.ritmintatorius.core.service.RRDataInterpreter
import org.apache.commons.csv.CSVRecord
import java.util.*

class AccuracyEstimateService(
    private val calculationVariables: Properties = Util().getCalculationVariables(2, 1),
    private val rrDataInterpreter: RRDataInterpreter = RRDataInterpreter().apply {
        this.calcVariables = calculationVariables
    },
    private val csvToIterableConverter: CsvToIterableConverter = CsvToIterableConverter()
) {

    fun measureExtrasystoles(
        rrIntervalLists: List<List<Float>>,
        disorderIndices: List<List<Int>> = emptyList(),
        ordinaryIndices: List<List<Int>> = emptyList(),
        sampleCount: Int = rrIntervalLists.size
    ): Double {

        if (disorderIndices.isNotEmpty() && ordinaryIndices.isNotEmpty())
            throw Exception("Cannot measure Alpha and Beta at the same time")
        var disorderIndicesIterator = disorderIndices.iterator()
        var ordinaryIndicesIterator = ordinaryIndices.iterator()
        return rrIntervalLists
            .map {
                val sublist =
                    getRelevantSublist(
                        it,
                        getIndicesFromIterator(disorderIndicesIterator),
                        getIndicesFromIterator(ordinaryIndicesIterator)
                    )
                rrDataInterpreter.getExtrasystoles(sublist).toList()
            }
            .map {
                disorderIndicesIterator = disorderIndices.iterator()
                ordinaryIndicesIterator = ordinaryIndices.iterator()
                it
            }
            .count {
                measureByCondition(
                    getIndicesFromIterator(disorderIndicesIterator),
                    getIndicesFromIterator(ordinaryIndicesIterator),
                    it
                )
            }
            .div(sampleCount.toDouble())
    }

    fun measureExtrasystoles(
        rrIntervalListsCsvFilename: String,
        disorderIndicesListFilename: String = "",
        ordinaryIndicesListFilename: String = ""
    ): Double {
        val rrIntervalLists = csvToIterableConverter.convert(rrIntervalListsCsvFilename).map { it.map { it.toFloat() } }
        val disorderIndicesLists =
            if (disorderIndicesListFilename == "")
                emptyList()
            else
                csvToIterableConverter.convert(disorderIndicesListFilename).map { getListFromCsvRow(it) }
        val ordinaryIndicesLists =
            if (ordinaryIndicesListFilename == "")
                emptyList()
            else
                csvToIterableConverter.convert(ordinaryIndicesListFilename).map { getListFromCsvRow(it) }
        println("D${disorderIndicesLists.size}")
        println("O${ordinaryIndicesLists.size}")
        return measureExtrasystoles(
            rrIntervalLists,
            disorderIndicesLists,
            ordinaryIndicesLists,
            rrIntervalLists.count()
        )
    }

    private fun getRelevantSublist(
        it: List<Float>, disorderIndices: List<Int>, ordinaryIndices: List<Int>
    ): List<Float> {
        var list = it
        val k = calculationVariables.getProperty("K").toInt()
        if (list.isEmpty())
            return list
        if (disorderIndices.isNotEmpty() && ordinaryIndices.isEmpty())
            list = list.subList(disorderIndices[0] - k, disorderIndices[1] + k + 1)
        if (disorderIndices.isEmpty() && ordinaryIndices.isNotEmpty())
            list = list.subList(ordinaryIndices[0] - k, ordinaryIndices[1] + k + 1)
        return list
    }

    private fun measureByCondition(
        expectedDisorderIndices: List<Int>, expectedOrdinaryIndices: List<Int>, foundDisorderIndices: List<Int>
    ): Boolean {
        val k = calculationVariables.getProperty("K").toInt()
        return if (expectedOrdinaryIndices.isNotEmpty()) {
            val alignedFoundIndices = foundDisorderIndices.map { it + expectedOrdinaryIndices[0] - k }
            getMeasuredBeta(alignedFoundIndices, expectedOrdinaryIndices)
        } else {
            var alignedFoundIndices = foundDisorderIndices
            if (expectedDisorderIndices.isNotEmpty())
                alignedFoundIndices = foundDisorderIndices.map { it + expectedDisorderIndices[0] - k }
            getMeasuredAlpha(alignedFoundIndices, expectedDisorderIndices)
        }
    }

    private fun getMeasuredAlpha(foundDisorderIndices: List<Int>, expectedDisorderIndices: List<Int>) =
        foundDisorderIndices.containsAll(expectedDisorderIndices)

    private fun getMeasuredBeta(foundDisorderIndices: List<Int>, expectedOrdinaryIndices: List<Int>) =
        !foundDisorderIndices.containsAll(expectedOrdinaryIndices)

    private fun getIndicesFromIterator(iterator: Iterator<List<Int>>) =
        if (iterator.hasNext())
            iterator.next()
        else emptyList()

    private fun getListFromCsvRow(it: CSVRecord) =
        try {
            it.map { it.toInt() }
        } catch (e: NumberFormatException) {
            emptyList<Int>()
        }
}
