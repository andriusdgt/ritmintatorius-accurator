package com.jaguarsoft.ritmintatorius.accurator.factory

import java.io.File
import java.io.FileReader

class FileReaderFactory(
    private val classLoader: ClassLoader = ClassLoader.getSystemClassLoader()
) {

    fun create(resourceFileName: String) = FileReader(File(classLoader.getResource(resourceFileName).toURI()))
}