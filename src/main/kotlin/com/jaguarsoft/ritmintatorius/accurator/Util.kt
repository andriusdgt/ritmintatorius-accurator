package com.jaguarsoft.ritmintatorius.accurator

import java.util.*

class Util(private val properties: Properties = Properties()) {

    fun getCalculationVariables(epsilon: Int, K: Int) =
        properties.apply {
            put("epsilon", (epsilon.toDouble() / 10.0).toString())
            put("K", K.toString())
        }

}