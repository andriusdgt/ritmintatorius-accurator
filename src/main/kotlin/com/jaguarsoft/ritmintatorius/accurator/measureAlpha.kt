package com.jaguarsoft.ritmintatorius.accurator

import com.jaguarsoft.ritmintatorius.accurator.service.AccuracyEstimateService

class AlphaCalculations {

    fun measure() {
        iterateThroughB(N_COUNT_STRING, DATA_SET_TYPE)
    }

    private fun iterateThroughB(nCountString: String, dataSetType: String) {
        for (bIterator in 0..3)
            iterateThroughK(nCountString, dataSetType, bIterator)
    }

    private fun iterateThroughK(nCountString: String, dataSetType: String, bIterator: Int) {
        for (KIterator in listOf(1, 3, 5, 10))
            iterateThroughEpsilon(nCountString, dataSetType, bIterator, KIterator)
    }

    private fun iterateThroughEpsilon(nCountString: String, dataSetType: String, bIterator: Int, KIterator: Int) {
        for (epsilonIterator in 1..9)
            measureAndPrintExtrasystoleWithDisordersAccuracy(nCountString, dataSetType, bIterator, epsilonIterator, KIterator)
    }

    private fun measureAndPrintExtrasystoleWithDisordersAccuracy(
        nCountString: String,
        dataSetType: String,
        bIterator: Int,
        epsilonIterator: Int,
        KIterator: Int
    ) {
        val csvFilename = "white,b=$bIterator/$nCountString/$dataSetType,b=$bIterator,RPins=0$epsilonIterator.csv"
//        val csvFilename = "white,b=2.0/$nCountString/$dataSetType,b=$bIterator,RPins=0$epsilonIterator.csv"
//        val csvFilename = "a9/$nCountString/$dataSetType,b=$bIterator,RPins=0$epsilonIterator.csv"
//    val csvFilename = "$N_COUNT_STRING/gen-sets_b=003263,a=9,$N_COUNT_STRING,m=102574,RPins=0$epsilonIterator.csv"

        println("Extrasystole detection accuracy for $csvFilename data set with epsilon=0.$epsilonIterator and K=$KIterator is:")
        println(
            AccuracyEstimateService(Util().getCalculationVariables(epsilonIterator, KIterator)).measureExtrasystoles(
                csvFilename, getCsvIndicesFilename(nCountString, dataSetType)
            )
        )
    }

    private fun getCsvIndicesFilename(nCountString: String, dataSetType: String) =
        "white,b=0/$nCountString/$nCountString,indices.csv"
//        "a9/$nCountString/$nCountString,indices.csv"

    companion object {
        const val N_COUNT_STRING = "n=100"
        const val K_STRING = "K=1"
        const val DATA_SET_TYPE = "white"
    }

}
