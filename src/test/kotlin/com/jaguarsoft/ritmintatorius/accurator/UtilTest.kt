package com.jaguarsoft.ritmintatorius.accurator

import org.junit.Assert.assertEquals
import org.junit.Test

class UtilTest {

    @Test
    fun `Configures calculation variables`() {
        for (K in 1..7) {
            for (epsilon in 1..9) {
                val properties = Util().getCalculationVariables(epsilon, K)

                assertEquals(epsilon.toDouble() / 10, properties.getProperty("epsilon").toDouble(), 0.000001)
                assertEquals(K, properties.getProperty("K").toInt())
            }
        }
    }

}