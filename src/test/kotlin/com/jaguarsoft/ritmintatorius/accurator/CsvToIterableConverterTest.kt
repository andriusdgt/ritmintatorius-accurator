package com.jaguarsoft.ritmintatorius.accurator

import com.jaguarsoft.ritmintatorius.accurator.factory.FileReaderFactory
import com.nhaarman.mockito_kotlin.*
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertSame
import java.io.FileReader

class CsvToIterableConverterTest {

    @Test
    fun `Given csv file name returns their iterable`() {
        val csvFileName = "data.csv"
        val csvFormatMock: CSVFormat = mock()
        val fileReaderFactoryMock: FileReaderFactory = mock()
        val csvParser = getCsvParser()
        doReturn(mock<FileReader>()).`when`(fileReaderFactoryMock).create(any())
        doReturn(csvParser).`when`(csvFormatMock).parse(any())

        val actualParser = CsvToIterableConverter(csvFormatMock, fileReaderFactoryMock).convert(csvFileName)

        assertSame(csvParser, actualParser)
        verify(csvFormatMock).parse(any())
        verify(fileReaderFactoryMock).create(csvFileName)
    }

    private fun getCsvParser(): CSVParser{
        val code = ("900.0,900.0,900.0\n" +
                "900.0,900.0,900.0\n" +
                "900.0,900.0,900.0\n")
        val format = CSVFormat.newFormat(',').withQuote('\'').withRecordSeparator('\n').withEscape('/').withIgnoreEmptyLines()
        return CSVParser.parse(code, format)
    }
}