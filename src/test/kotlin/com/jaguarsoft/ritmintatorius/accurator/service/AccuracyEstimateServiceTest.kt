package com.jaguarsoft.ritmintatorius.accurator.service

import com.jaguarsoft.ritmintatorius.accurator.CsvToIterableConverter
import com.nhaarman.mockito_kotlin.*
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class AccuracyEstimateServiceTest {

    @Mock
    private lateinit var converterStub: CsvToIterableConverter

    private lateinit var service: AccuracyEstimateService

    @Rule
    @JvmField
    var expectedException = ExpectedException.none()!!

    @Before
    fun setUp() {
        service = AccuracyEstimateService(csvToIterableConverter = converterStub)
    }

    @Test
    fun `Measures 100% extrasystole detection accuracy provided samples with no extrasytoles (true negatives case, Alpha)`() {
        val healthySamples = List(10) { List(60) { 0.9f } }
        val disorderIndicesList = emptyList<List<Int>>()

        val accuracy = service.measureExtrasystoles(healthySamples, disorderIndicesList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Measures 100% extrasystole detection accuracy provided samples with extrasytoles (true positives case, Alpha)`() {
        val samples = listOf(
            List(10) { List(60) { 0.9f } },
            List(10) {
                listOf(
                    List(40) { 0.9f },
                    listOf(0.4f, 1.8f),
                    List(18) { 0.9f }
                ).flatten()
            }
        ).flatten()
        val disorderIndicesList = listOf(
            List(10) { emptyList<Int>() },
            List(10) { listOf(40, 41) }
        ).flatten()

        val accuracy = service.measureExtrasystoles(samples, disorderIndicesList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Measures 50% extrasystole detection accuracy provided half samples with false negatives (calculates Alpha)`() {
        val healthySamples = List(20) { List(60) { 0.9f } }
        val disorderIndicesList = listOf(
            List(10) { listOf(40, 41) },
            List(10) { emptyList<Int>() }
        ).flatten()

        val accuracy = service.measureExtrasystoles(healthySamples, disorderIndicesList)

        assertEquals(0.5, accuracy)
    }

    @Test
    fun `Measures 50% extrasystole detection accuracy provided half samples with false positives (calculates Beta)`() {
        val samples = listOf(
            List(10) { List(60) { 0.9f } },
            List(10) {
                listOf(
                    List(40) { 0.9f },
                    listOf(0.4f, 1.8f),
                    List(18) { 0.9f }
                ).flatten()
            }
        ).flatten()
        val disorderIndicesList = emptyList<List<Int>>()
        val ordinaryIndicesList = List(20) { listOf(40, 41) }

        val accuracy = service.measureExtrasystoles(samples, disorderIndicesList, ordinaryIndicesList)

        assertEquals(0.5, accuracy)
    }

    @Test
    fun `Accepts disorder csv file as input`() {
        val healthySampleListFilename = "data.csv"
        val disorderIndicesListFilename = "disorderIndices.csv"
        doReturn(getCsvParserForHealthySampleData()).`when`(converterStub).convert(healthySampleListFilename)
        doReturn(getCsvParser("")).`when`(converterStub).convert(disorderIndicesListFilename)

        val accuracy = service.measureExtrasystoles(healthySampleListFilename, disorderIndicesListFilename)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Accepts ordinary index csv file as input`() {
        val healthySampleListFilename = "data.csv"
        val ordinaryIndicesListFilename = "ordinaryIndices.csv"
        doReturn(getCsvParserForHealthySampleData()).`when`(converterStub).convert(healthySampleListFilename)
        doReturn(getCsvParser("")).`when`(converterStub).convert(ordinaryIndicesListFilename)

        val accuracy = service.measureExtrasystoles(healthySampleListFilename, "", ordinaryIndicesListFilename)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Measures 50% extrasystole detection accuracy with csv data which includes false positives (measures Beta)`() {
        val sampleFilename = "data.csv"
        val ordinaryIndicesListFilename = "ordinaryIndices.csv"
        doReturn(getCsvParserForSampleDataWithExtrasystoles()).`when`(converterStub).convert(sampleFilename)
        doReturn(getCsvParserForExtrasystoleIndices()).`when`(converterStub).convert(ordinaryIndicesListFilename)

        val accuracy = service.measureExtrasystoles(sampleFilename, "", ordinaryIndicesListFilename)

        assertEquals(0.5, accuracy)
    }

    @Test
    fun `Measures 100% extrasystole detection accuracy with csv data which includes true positives (measures Alpha)`() {
        val sampleFilename = "data.csv"
        val disorderIndicesListFilename = "disorderIndices.csv"
        doReturn(getCsvParserForSampleDataWithExtrasystoles()).`when`(converterStub).convert(sampleFilename)
        doReturn(getCsvParserForExtrasystoleIndices()).`when`(converterStub).convert(disorderIndicesListFilename)

        val accuracy = service.measureExtrasystoles(sampleFilename, disorderIndicesListFilename)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Given empty samples returns 100% detection accuracy`() {
        val emptySampleList = List(10) { emptyList<Float>() }
        val emptyIndicesList = List(10) { emptyList<Int>() }

        val accuracy = service.measureExtrasystoles(emptySampleList, emptyIndicesList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Accuracy is not influenced by other false positives as long as single correct true positive is found`() {
        val samples = listOf(
            listOf(
                List(3) { 0.9f },
                listOf(0.4f, 1.8f),
                List(3) { 0.9f },
                listOf(0.4f, 1.8f),
                List(3) { 0.9f }
            ).flatten()
        )
        val disorderIndicesList = listOf(listOf(3, 4))

        val accuracy = service.measureExtrasystoles(samples, disorderIndicesList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Accuracy is not influenced by other true negatives as long as single checked instance is true`() {
        val samples = listOf(
            listOf(
                List(6) { 0.9f },
                listOf(0.4f, 1.8f),
                List(6) { 0.9f }
            ).flatten()
        )
        val ordinaryIndicesList = listOf(listOf(3, 4))

        val accuracy = service.measureExtrasystoles(samples, emptyList(), ordinaryIndicesList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Measures accuracy without providing disorderIndices`() {
        val healthySampleList = List(10) { List(60) { 0.9f } }

        val accuracy = service.measureExtrasystoles(healthySampleList)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Accepts partial disorderIndices list`() {
        val healthySampleList = List(20) { List(60) { 0.9f } }
        val disorderIndicesList = listOf(
            List(10) { listOf(40, 41) }
        )
            .flatten()

        val accuracy = service.measureExtrasystoles(healthySampleList, disorderIndicesList)

        assertEquals(0.5, accuracy)
    }

    @Test
    fun `Measures accuracy with custom calculation variables`() {
        val healthySampleList = List(10) { List(60) { 0.9f } }
        val calcVariablesSpy: Properties = spy()
        doReturn("0.2").`when`(calcVariablesSpy).getProperty("epsilon")
        doReturn("1").`when`(calcVariablesSpy).getProperty("K")

        val accuracy = AccuracyEstimateService(calcVariablesSpy).measureExtrasystoles(healthySampleList)

        verify(calcVariablesSpy, atLeastOnce()).getProperty(any())
        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Custom calculation variables influence measured accuracy results`() {
        val unhealthySampleList = listOf(listOf(0.9f, 0.9f, 0.9f, 0.01f, 3.0f, 0.9f))
        val disorderIndicesList = listOf(listOf(3, 4))
        val calcVariablesSpy: Properties = spy()
        doReturn("0.2").`when`(calcVariablesSpy).getProperty("epsilon")
        doReturn("1").`when`(calcVariablesSpy).getProperty("K")

        val accuracy = AccuracyEstimateService(calcVariablesSpy)
            .measureExtrasystoles(unhealthySampleList, emptyList(), disorderIndicesList)

        assertEquals(0.0, accuracy)
    }

    @Test
    fun `Accepts missing disorder csv filename as input (calculates Alpha)`() {
        val sampleFilename = "data.csv"
        doReturn(getCsvParserForHealthySampleData()).`when`(converterStub).convert(sampleFilename)

        val accuracy = service.measureExtrasystoles(sampleFilename)

        assertEquals(1.00, accuracy)
    }

    @Test
    fun `Should get an exception if attempting to measure Alpha and Beta at the same time`() {
        val sampleFilename = "data.csv"
        val disorderIndicesFilename = "disorderIndices.csv"
        val ordinaryIndicesFilename = "ordinaryIndices.csv"
        expectedException.expect(Exception::class.java)
        expectedException.expectMessage("Cannot measure Alpha and Beta at the same time")
        doReturn(getCsvParserForHealthySampleData()).`when`(converterStub).convert(sampleFilename)
        doReturn(getCsvParserForExtrasystoleIndices()).`when`(converterStub).convert(disorderIndicesFilename)
        doReturn(getCsvParserForExtrasystoleIndices()).`when`(converterStub).convert(ordinaryIndicesFilename)

        service.measureExtrasystoles(sampleFilename, disorderIndicesFilename, ordinaryIndicesFilename)
    }

    private fun getCsvParserForHealthySampleData(): CSVParser {
        val code = ("0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n")
        return getCsvParser(code)
    }

    private fun getCsvParserForSampleDataWithExtrasystoles(): CSVParser {
        val code = ("0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.3,1.8,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.3,1.8,0.9,0.9,0.9,0.9\n" +
            "0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9\n")
        return getCsvParser(code)
    }

    private fun getCsvParserForExtrasystoleIndices(): CSVParser {
        val code = ("\n" +
            "4,5\n" +
            "4,5\n" +
            "\n")
        return getCsvParser(code)
    }

    private fun getCsvParser(code: String): CSVParser {
        val format =
            CSVFormat.newFormat(',').withQuote('\'').withRecordSeparator('\n').withEscape('/')
        return CSVParser.parse(code, format)
    }

}
